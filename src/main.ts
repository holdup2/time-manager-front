import './assets/main.css'

import { createApp } from 'vue'
import Vuex from 'vuex';
import App from './App.vue'
import router from './router'
import 'flowbite';

import PrimeVue from 'primevue/config';
import Calendar from "primevue/calendar";
import 'primevue/resources/themes/lara-light-teal/theme.css';
import Button from "primevue/button";
import DataTable from "primevue/datatable";
import Column from "primevue/column";
import Card from "primevue/card";
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import RadioButton from "primevue/radiobutton";
import Accordion from "primevue/accordion";
import AccordionTab from "primevue/accordiontab";
import VueApexCharts from 'vue3-apexcharts'
import InputText from "primevue/inputtext";
import TabView from "primevue/tabview";
import TabPanel from "primevue/tabpanel";
import store from "./store";

const app = createApp(App);


app.component('p-calendar',Calendar)
app.component('p-button',Button)
app.component('p-table',DataTable)
app.component('p-column',Column)
app.component("p-card", Card)
app.component('p-toast', Toast)
app.component('p-radio-button', RadioButton)
app.component('p-accordion',Accordion)
app.component('p-accordion-tab',AccordionTab)
app.component('p-input-text', InputText)
app.component('p-tabview', TabView)
app.component('p-tabpannel',TabPanel)
app.use(VueApexCharts);
app.use(ToastService);
app.use(PrimeVue);
app.use(router);
app.use(Vuex);
app.use(store);
app.mount('#app')
