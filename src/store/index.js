import Vuex from "vuex";

const store = new Vuex.Store({
    state: {
        user: {
            id : "1",
            username: 'matt',
            email: 'matt@gmail.com',
        },
        token: '',
        connected : false,
    },
    getters: {
        fullUser : state => {
            return state.user
        },
        getToken : state => {
            return state.token
        },
        getConnected : state => {
            return state.connected
        }
    },
    mutations: {
        changeUser (state, payload) {
            state.user = payload
        },
        changeToken (state, payload) {
            state.token = payload
        },
        changeConnected (state, payload) {
            state.connected = payload
        }
    },
    actions: {}
});

export default store;