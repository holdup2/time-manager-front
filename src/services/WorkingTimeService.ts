import axios from "axios";
import type {WorkingTimeModel} from "@/models/workingTime-model";

const workingTimeService = {
    async GetWorkingtimeByUser(user_id: string, start: string, end: string) {
        const url = 'http://localhost:4000/api/workingtimes/' + user_id;
        const params = new URLSearchParams();
        params.append("start", start)
        params.append("end", end)
        try {
            const response = await axios.get(url, {params: params});
            return response.data.data;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },
    async GetWorkingtimeById(user_id: string, id: string) {
        const url = 'http://localhost:4000/api/workingtimes/' + user_id + '/' + id;
        try {
            const response = await axios.get(url);
            return response.data.data;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },
    async CreateWorkingtime(user_id: string, workingtime : WorkingTimeModel) {
        const url = 'http://localhost:4000/api/workingtimes/'+user_id;
        return await axios.post(url, workingtime);
    },

    async UpdateWorkingtime(id: string, workingtime: WorkingTimeModel) {
        const url = 'http://localhost:4000/api/workingtimes/'+id;
        return await axios.put(url, workingtime);
    },
    async DeleteWorkingtime(id : string) {
        const url = 'http://localhost:4000/api/workingtimes/'+id;
        return await axios.delete(url);
    }
}

export default workingTimeService;