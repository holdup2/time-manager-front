import axios from "axios";

const clockService = {
    async getClock(id : string) {
        const url = 'http://localhost:4000/api/clocks/' + id;
        try {
            const response = await axios.get(url);
            return response.data.data;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },
    async createClock(user_id: string, clock) {
        const url = 'http://localhost:4000/api/clocks/'+user_id;
        return await axios.post(url, clock);
    },
}

export default clockService