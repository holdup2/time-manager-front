import axios from "axios";

const back = "http://localhost:4000"

const userService = {

    async GetUser(id : string) {
        const url = back +'/api/users/' + id;
        try {
            const response = await axios.get(url);
            return response.data.data;
        } catch (err) {
            console.log(err);
            throw err;
        }
    },

    async GetUserByCriteria(criteria: string, filter: string) {
        const url = back +'/api/users/';
        const params = new URLSearchParams();
        params.append(criteria, filter);
        return axios.get(url, { params: params })
    },
    async CreateUser(user) {
        const url = back +'/api/users/';
        return await axios.post(url, user);
    },
    async DeleteUser(id : string) {
        const url = back +'/api/users/'+id;
        return await axios.delete(url);
    },
    async UpdateUser(id : string, user) {
        const url = back +'/api/users/'+id;
        return await axios.put(url, user);
    }

}

export default userService;
