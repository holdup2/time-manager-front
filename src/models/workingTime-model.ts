export interface WorkingTimeModel {
    workingtime : {
        start: string,
        end: string,
    }
}