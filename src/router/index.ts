import { createRouter, createWebHistory } from 'vue-router'
import WorkingTime from '../components/WorkingTime.vue'
import WorkingTimes from '../components/WorkingTimes.vue'
import ClockManager from "@/components/ClockManager.vue";
import ChartManager from "@/components/ChartManager.vue";
import Login from "../views/Login.vue";
import HomeView from "../views/HomeView.vue";
import Register from "../views/Register.vue";
import UserComponent from "@/components/UserComponent.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
      {
      path: '/home',
      name: 'home',
      component: HomeView

    },

    {
      path: '/',
      redirect: '/login'
    },


    {
      path: '/workingtime/:user_id',
      name: 'workingtimes',
      component: WorkingTimes
    },
    {
      path: '/workingtime/:user_id/:workingtime_id',
      name: 'workingtime',
      component: WorkingTime
    },
    {
      path: '/clock/:username',
      name: 'clock',
      component: ClockManager
    },
    {
      path: '/chartmanager/:user_id',
      name: 'chartmanager',
      component: ChartManager
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    }
  ]
})

export default router
